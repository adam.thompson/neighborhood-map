<?php
/**
 * Plugin Name: Interactive Neighborhood Map
 * Plugin URI: ''
 * Description: This plugin registers the locations cpt and allows you add the interactive map to pages/posts via the [neighborhood-map] shortcode.
 * Version: 1.0
 * Author: Tenderling Design
 * Author URI: https://tenderling.com
 *
 */

 //include plugin settings
include 'inc/plugin-settings.php';
//include location cpt
include 'inc/locations-cpt.php';
//include admin styles
include 'inc/plugin-admin-styles.php';
//include shortcode
include 'inc/shortcode.php';
