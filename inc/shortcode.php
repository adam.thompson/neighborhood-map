<?php
//function for registering our [neighborhood_map] shortcode
function neighborhood_map_shortcode($atts = false) {
	$filter = false;
	$show_locations = true;
	$zoom = false;

	if ($atts) {
		if (isset($atts['filter-by-slug'])) {
			$filter = $atts['filter-by-slug'];
		}

		if (isset($atts['show-locations'])) {
			if ($atts['show-locations'] == 'false') {
				$show_locations = false;
			}
		}

		if (isset($atts['zoom'])) {
			$zoom = $atts['zoom'];
		}
	}

	$location_types = get_terms(array( 'taxonomy' => 'location-type' ));
	$filter_alignment = get_option( 'map_filter_alignment' ) ?: false;
	$alphabetical = get_option( 'alphabetical' ) ?: false;

	if ($show_locations) {
		echo (
			"<div class='section google-map filter-$filter_alignment[filter_alignment_radio]' data-filter='$filter' data-zoom='$zoom'>
				<div class='map' style='min-height: 450px;'></div>");
			echo "<ul class='map-filters'>";
				echo "<li title='All' id='All'><div tabindex='0' class='active' id='location-heading'>All</div></li>";
				foreach ($location_types as $location) {
					if (isset($alphabetical['alphabetical_radio']) && $alphabetical['alphabetical_radio'] != 'no') {
						$args = array(
							'post_type' => 'locations',
							'post_status' => 'publish',
							'posts_per_page' => 100,
							'orderby' => 'title',
							'order' => 'ASC',
							'tax_query' => array(
								array(
									'taxonomy' => 'location-type',
									'field' => 'slug',
									'terms' => array($location->slug)
								),
							),
						);
					} else {
						$args = array(
							'post_type' => 'locations',
							'post_status' => 'publish',
							'posts_per_page' => 100,
							'tax_query' => array(
								array(
									'taxonomy' => 'location-type',
									'field' => 'slug',
									'terms' => array($location->slug)
								),
							),
						);
					}
					$query = new WP_Query( $args );
					$tax_count = $query->found_posts;

					if($tax_count > 0) {
						echo "<li id='{$location->slug}' title='{$location->name}' data-list=".get_option('show_location_list')['show_location_list_radio'].">";
							if ($filter_alignment["filter_alignment_radio"] != 'bottom') {
								echo "<div tabindex='0' id='location-heading' class='active' style='background: ".get_field('color', $location->taxonomy.'_'.$location->term_id)."'>";
							} else {
								if (get_option('location_style')['location_style_radio'] == 'icon') {
									echo "<div tabindex='0' id='location-heading' class='icon'>";
								} else {
									echo "<div tabindex='0' id='location-heading' style='background: ".get_field('color', $location->taxonomy.'_'.$location->term_id)."'>";
								}
							}
							if (get_option('location_style')['location_style_radio'] == 'icon') {
								echo "<img class='category-icon' src='".wp_get_attachment_image_src(get_field('icon', $location->taxonomy.'_'.$location->term_id))[0]."' />";
							}
							echo "{$location->name}</div>";
								//If we want to bring back post count add this span next to the {$location->name} above <span class='count'>({$tax_count})</span>
							if (get_option('show_location_list')['show_location_list_radio'] == 'true') {
								if ($filter_alignment["filter_alignment_radio"] != 'bottom') {
									echo "<ol id='location-list' class='active'>";
								} else {
									echo "<ol id='location-list'>";
								}
									while ($query->have_posts()) : ?>
										<?php $query->the_post(); ?>
										<li class="map-location" id="<?php echo get_the_ID(); ?>" data-lat="<?php echo get_field('latitude') ?: false; ?>" data-lng="<?php echo get_field('longitude') ?: false; ?>"><?php the_title(); ?></li>
									<?php endwhile;
									wp_reset_query(); //necessary if there's page content beyond the shortcode
								echo "</ol>";
							}
						echo "</li>";
					}
				}
			echo "</ul>";
			echo "<div id='map-stylings' style='display: none'>";
				echo json_encode([
					'highway_fill' => get_option( 'map_color_highway_fill' ),
					'highway_outline' => get_option( 'map_color_highway_outline' ),
					'road_city_fill' => get_option( 'map_color_road_city_fill' ),
					'road_city_outline' => get_option( 'map_color_road_city_outline' ),
					'road_arterial_fill' => get_option( 'map_color_road_arterial_fill' ),
					'road_arterial_outline' => get_option( 'map_color_road_arterial_outline' ),
					'road_label' => get_option( 'map_color_road_label' ),
					'state_label' => get_option( 'map_color_state_label' ),
					'landscape' => get_option( 'map_color_landscape' ),
					'water' => get_option( 'map_color_water' ),
					'zoom' => get_option( 'default_zoom' ),
					'tablet_zoom' => get_option( 'default_tablet_zoom' ),
					'mobile_zoom' => get_option( 'default_mobile_zoom' ),
					'location_style' => get_option( 'location_style' )['location_style_radio'] ?: false,
					'country_border' => get_option('country_border')['country_border'] ?: false,
					'fitbounds' => get_option('category_fitbounds')['category_fitbounds_radio'] ?: false,
					'show_poi' => get_option('show_poi')['show_poi_radio'] ?: false,
				]);
			echo "</div>";
			//add inline stylesheet
			echo "<style>".file_get_contents(plugins_url().'/neighborhood-map/css/neighborhood-map-filters-'.$filter_alignment['filter_alignment_radio'].'.css')."</style>";
		echo "</div>";
	} else {
		echo (
			"<div class='section google-map filter-$filter_alignment[filter_alignment_radio]' data-filter='$filter' data-show-locations='$show_locations'>
				<div class='map' style='min-height: 450px;'></div>");
				echo "<div id='map-stylings' style='display: none'>";
				echo json_encode([
					'highway_fill' => get_option( 'map_color_highway_fill' ),
					'highway_outline' => get_option( 'map_color_highway_outline' ),
					'road_city_fill' => get_option( 'map_color_road_city_fill' ),
					'road_city_outline' => get_option( 'map_color_road_city_outline' ),
					'road_arterial_fill' => get_option( 'map_color_road_arterial_fill' ),
					'road_arterial_outline' => get_option( 'map_color_road_arterial_outline' ),
					'road_label' => get_option( 'map_color_road_label' ),
					'state_label' => get_option( 'map_color_state_label' ),
					'landscape' => get_option( 'map_color_landscape' ),
					'water' => get_option( 'map_color_water' ),
					'zoom' => get_option( 'default_zoom' ),
					'tablet_zoom' => get_option( 'default_tablet_zoom' ),
					'mobile_zoom' => get_option( 'default_mobile_zoom' ),
					'location_style' => get_option( 'location_style' )['location_style_radio'] ?: false,
					'country_border' => get_option('country_border')['country_border'] ?: false,
					'fitbounds' => get_option('category_fitbounds')['category_fitbounds_radio'] ?: false,
				]);
			echo "</div>";
			//add inline stylesheet
			echo "<style>".file_get_contents(plugins_url().'/neighborhood-map/css/neighborhood-map-filters-'.$filter_alignment['filter_alignment_radio'].'.css')."</style>";
		echo "</div>";


	}
}
add_shortcode( 'neighborhood_map', 'neighborhood_map_shortcode' );
