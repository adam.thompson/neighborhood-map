<?php
//add css for admin styling
function neighborhood_map_admin_style($hook_suffix) {
	wp_enqueue_style('neighborhood_map_admin_style', plugins_url().'/neighborhood-map/css/neighborhood-map-admin.css');
	if ($hook_suffix == 'toplevel_page_neighborhood-map/inc/plugin-settings') {
		wp_enqueue_style( 'wp-color-picker' );
		wp_enqueue_script( 'wp-color-picker');
		wp_enqueue_script( 'neighborhood_map_admin_script', plugins_url().'/neighborhood-map/js/neighborhood-map-admin.js', array('jquery', 'wp-color-picker'), false, true );
		wp_enqueue_media();
	}
}
add_action('admin_enqueue_scripts', 'neighborhood_map_admin_style');
