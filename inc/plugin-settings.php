<?php
// create custom plugin settings menu
add_action('admin_menu', 'neighborhood_map_plugin_create_menu');

function neighborhood_map_plugin_create_menu() {
	//create new top-level menu
	add_menu_page('Neighborhood Map Settings', 'Neighborhood Map Settings', 'administrator', __FILE__, 'neighborhood_map_plugin_settings_page' , plugins_url('../images/icon.png', __FILE__) );
	//call register settings function
	add_action( 'admin_init', 'neighborhood_map_plugin_settings' );
}

add_action('load-toplevel_page_neighborhood-map/inc/plugin-settings', 'custom_neighborhood_add_default_location_lat_lng');

function custom_neighborhood_add_default_location_lat_lng() {
	if (isset($_GET['settings-updated']) && $_GET['settings-updated']) {
		if (get_option('default_location_address') != '' && get_option('google_maps_api_key') != '') {
			if (get_option('default_location_lat') == '' || get_option('default_location_lng') == '') {
				$url = "https://maps.google.com/maps/api/geocode/json?address=".urlencode(get_option('default_location_address'))."&sensor=false&key=".get_option('google_maps_api_key');

				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
				$response = curl_exec($ch);
				curl_close($ch);

				$json = json_decode($response);

				update_option('default_location_lat', $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'});
				update_option('default_location_lng', $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'});
			}
		}
	}
}

function neighborhood_map_image_uploader() {
	$default_image_id = get_option( 'default_location_icon' );
    $default_image = wp_get_attachment_image_src($default_image_id)[0] ?: false;

    // Print HTML field
    echo '<div class="upload">
            <img data-src="" src="'.$default_image.'" width="100px" height="auto" />
            <div>
                <input type="hidden" name="default_location_icon" id="default_location_icon" value="' . $default_image_id . '" />
                <button type="submit" class="upload_image_button button">Upload</button>
                <button type="submit" class="remove_image_button button">&times;</button>
            </div>
		</div>';
	echo "<script>
			// The 'Upload' button
			jQuery('.upload_image_button').click(function() {
				var send_attachment_bkp = wp.media.editor.send.attachment;
				var button = jQuery(this);
				wp.media.editor.send.attachment = function(props, attachment) {
					jQuery(button).parent().prev().attr('src', attachment.url);
					jQuery(button).prev().val(attachment.id);
					wp.media.editor.send.attachment = send_attachment_bkp;
				}
				wp.media.editor.open(button);
				return false;
			});

			// The 'Remove' button (remove the value from input type='hidden')
			jQuery('.remove_image_button').click(function() {
				var answer = confirm('Are you sure?');
				if (answer == true) {
					var src = jQuery(this).parent().prev().attr('data-src');
					jQuery(this).parent().prev().attr('src', src);
					jQuery(this).prev().prev().val('');
				}
				return false;
			});
		</script>";
}

function neighborhood_map_plugin_settings() {
	//register our settings
	register_setting( 'neighborhood-map-settings-group', 'google_maps_api_key' );
	register_setting( 'neighborhood-map-settings-group', 'default_zoom' );
	register_setting( 'neighborhood-map-settings-group', 'max_lat' );
	register_setting( 'neighborhood-map-settings-group', 'min_lat' );
	register_setting( 'neighborhood-map-settings-group', 'max_lng' );
	register_setting( 'neighborhood-map-settings-group', 'min_lng' );
	register_setting( 'neighborhood-map-settings-group', 'default_tablet_zoom' );
	register_setting( 'neighborhood-map-settings-group', 'default_mobile_zoom' );
	register_setting( 'neighborhood-map-settings-group', 'default_location_address' );
	register_setting( 'neighborhood-map-settings-group', 'default_location_title' );
	register_setting( 'neighborhood-map-settings-group', 'default_location_website' );
	register_setting( 'neighborhood-map-settings-group', 'default_location_phone_number' );
	register_setting( 'neighborhood-map-settings-group', 'default_location_icon' );
	register_setting( 'neighborhood-map-settings-group', 'default_location_width' );
	register_setting( 'neighborhood-map-settings-group', 'default_location_height' );
	register_setting( 'neighborhood-map-settings-group', 'default_location_lat' );
	register_setting( 'neighborhood-map-settings-group', 'default_location_lng' );
	register_setting( 'neighborhood-map-settings-group', 'map_color_highway_fill' );
	register_setting( 'neighborhood-map-settings-group', 'map_color_highway_outline' );
	register_setting( 'neighborhood-map-settings-group', 'map_color_road_city_fill' );
	register_setting( 'neighborhood-map-settings-group', 'map_color_road_city_outline' );
	register_setting( 'neighborhood-map-settings-group', 'map_color_road_arterial_fill' );
	register_setting( 'neighborhood-map-settings-group', 'map_color_road_arterial_outline' );
	register_setting( 'neighborhood-map-settings-group', 'map_color_road_label' );
	register_setting( 'neighborhood-map-settings-group', 'map_color_state_label' );
	register_setting( 'neighborhood-map-settings-group', 'map_color_landscape' );
	register_setting( 'neighborhood-map-settings-group', 'map_color_water' );
	register_setting( 'neighborhood-map-settings-group', 'map_filter_alignment' );
	register_setting( 'neighborhood-map-settings-group', 'location_style' );
	register_setting( 'neighborhood-map-settings-group', 'click_to_center' );
	register_setting( 'neighborhood-map-settings-group', 'show_location_list' );
	register_setting( 'neighborhood-map-settings-group', 'country_border' );
	register_setting( 'neighborhood-map-settings-group', 'category_fitbounds' );
	register_setting( 'neighborhood-map-settings-group', 'single_location_page' );
	register_setting( 'neighborhood-map-settings-group', 'alphabetical' );
	register_setting( 'neighborhood-map-settings-group', 'show_poi' );
}

function neighborhood_map_plugin_settings_page() {
?>
	<div class="wrap">
		<h1>Neighborhood Map Settings</h1>
		<div class="explanation"><strong>How to use:</strong> This plugin registers the shortcode [neighborhood_map] that can be dropped into a page or a post</div>
		<div class="explanation">
			<p>The shortcode has a few settings to be aware of:</p>
			<ul>
				<li>[neighborhood-map filter-by-slug=`any-location-category-slug`]</li>
				<li>[neighborhood-map show-locations=`true|false`]</li>
				<li>[neighborhood-map zoom=`int`]</li>
			</ul>
		</div>
		<form method="post" action="options.php">
			<?php settings_fields( 'neighborhood-map-settings-group' ); ?>
			<?php do_settings_sections( 'neighborhood-map-settings-group' ); ?>
			<div class="form-holder row">
				<table class="form-table map-settings col-sm-6">
					<thead><tr><th colspan="2"><h3>Map Settings</h3></th></tr></thead>
					<tbody>
						<tr valign="top">
							<th scope="row">Google Maps API Key:</th>
							<td><input type="text" name="google_maps_api_key" value="<?php echo esc_attr( get_option('google_maps_api_key') ); ?>" /></td>
						</tr>
						<tr valign="top">
							<th scope="row">Location Settings</th>
							<td><p>Please choose whether to use the location type icon or color:</p>
							<?php $location_style = get_option( 'location_style' ) ?: false; ?>
								<table>
									<tbody>
										<tr>
											<td style="padding: 0 5px 5px"><label for="location_style_radio_one">Color</label></td>
											<td style="padding: 0 5px 5px"><input type="radio" id="location_style_radio_one" name="location_style[location_style_radio]" value="color" <?php echo $location_style ? checked( 'color', $location_style['location_style_radio'], false ) : false; ?> /></td>
										</tr>
										<tr>
											<td style="padding: 0 5px 5px"><label for="location_style_radio_two">Icon</label></td>
											<td style="padding: 0 5px 5px"><input type="radio" id="location_style_radio_two" name="location_style[location_style_radio]" value="icon" <?php echo $location_style ? checked( 'icon', $location_style['location_style_radio'], false ) : false; ?> /></td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr valign="top">
							<th scope="row"></th>
							<td><p>Please choose whether to center map on the clicked location:</p>
							<?php $click_to_center = get_option( 'click_to_center' ) ?: false; ?>
								<table>
									<tbody>
										<tr>
											<td style="padding: 0 5px 5px"><label for="click_to_center_radio_one">Yes</label></td>
											<td style="padding: 0 5px 5px"><input type="radio" id="click_to_center_radio_one" name="click_to_center[click_to_center_radio]" value="yes" <?php echo $click_to_center ? checked( 'yes', $click_to_center['click_to_center_radio'], false ) : false; ?> /></td>
										</tr>
										<tr>
											<td style="padding: 0 5px 5px"><label for="click_to_center_radio_two">No</label></td>
											<td style="padding: 0 5px 5px"><input type="radio" id="click_to_center_radio_two" name="click_to_center[click_to_center_radio]" value="no" <?php echo $click_to_center ? checked( 'no', $click_to_center['click_to_center_radio'], false ) : false; ?> /></td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr valign="top">
							<th scope="row"></th>
							<td><p>Please choose whether to have the location list be alphabetical or not:</p>
							<?php $alphabetical = get_option( 'alphabetical' ) ?: false; ?>
								<table>
									<tbody>
										<tr>
											<td style="padding: 0 5px 5px"><label for="alphabetical_radio_one">Yes</label></td>
											<td style="padding: 0 5px 5px"><input type="radio" id="alphabetical_radio_one" name="alphabetical[alphabetical_radio]" value="yes" <?php echo $alphabetical ? checked( 'yes', $alphabetical['alphabetical_radio'], false ) : false; ?> /></td>
										</tr>
										<tr>
											<td style="padding: 0 5px 5px"><label for="alphabetical_radio_two">No</label></td>
											<td style="padding: 0 5px 5px"><input type="radio" id="alphabetical_radio_two" name="alphabetical[alphabetical_radio]" value="no" <?php echo $alphabetical ? checked( 'no', $alphabetical['alphabetical_radio'], false ) : false; ?> /></td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr valign="top">
							<th scope="row">Boundary Constraints</th>
							<td>
								<table>
									<tbody>
										<tr><td style="padding: 3px;" colspan="2" cellpadding="0"><strong>Latitiude</strong></td></tr>
										<tr>
											<td>Max: <input type="text" name="max_lat" value="<?php echo esc_attr( get_option('max_lat') ) ?: false; ?>" /></td>
											<td>Min: <input type="text" name="min_lat" value="<?php echo esc_attr( get_option('min_lat') ) ?: false; ?>" /></td>
										</tr>
										<tr><td style="padding: 3px;" colspan="2" cellpadding="0"><strong>Longitude</strong></td></tr>
										<tr>
											<td>Max: <input type="text" name="max_lng" value="<?php echo esc_attr( get_option('max_lng') ) ?: false; ?>" /></td>
											<td>Min: <input type="text" name="min_lng" value="<?php echo esc_attr( get_option('min_lng') ) ?: false; ?>" /></td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr valign="top">
							<th scope="row"></th>
							<td><p>Allow for single location page?</p>
							<?php $show_single = get_option( 'single_location_page' ) ?: false; ?>
								<table>
									<tbody>
										<tr>
											<td style="padding: 0 5px 5px"><label for="single_location_page_radio_one">Yes</label></td>
											<td style="padding: 0 5px 5px"><input type="radio" id="single_location_page_radio_one" name="single_location_page[single_location_page_radio]" value="true" <?php echo $show_single ? checked( 'true', $show_single['single_location_page_radio'], false ) : false; ?> /></td>
										</tr>
										<tr>
											<td style="padding: 0 5px 5px"><label for="single_location_page_radio_two">No</label></td>
											<td style="padding: 0 5px 5px"><input type="radio" id="single_location_page_radio_two" name="single_location_page[single_location_page_radio]" value="false" <?php echo $show_single ? checked( 'false', $show_single['single_location_page_radio'], false ) : false; ?> /></td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr valign="top">
							<th scope="row"></th>
							<td><p>Show locations list below category filter?</p>
							<?php $show_list = get_option( 'show_location_list' ) ?: false; ?>
								<table>
									<tbody>
										<tr>
											<td style="padding: 0 5px 5px"><label for="show_location_list_radio_one">Yes</label></td>
											<td style="padding: 0 5px 5px"><input type="radio" id="show_location_list_radio_one" name="show_location_list[show_location_list_radio]" value="true" <?php echo $show_list ? checked( 'true', $show_list['show_location_list_radio'], false ) : false; ?> /></td>
										</tr>
										<tr>
											<td style="padding: 0 5px 5px"><label for="show_location_list_radio_two">No</label></td>
											<td style="padding: 0 5px 5px"><input type="radio" id="show_location_list_radio_two" name="show_location_list[show_location_list_radio]" value="false" <?php echo $show_list ? checked( 'false', $show_list['show_location_list_radio'], false ) : false; ?> /></td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr valign="top">
							<th scope="row"></th>
							<td><p>Have map fit bounds of selected category?</p>
							<?php $fitbounds = get_option( 'category_fitbounds' ) ?: false; ?>
								<table>
									<tbody>
										<tr>
											<td style="padding: 0 5px 5px"><label for="category_fitbounds_one">Yes</label></td>
											<td style="padding: 0 5px 5px"><input type="radio" id="category_fitbounds_one" name="category_fitbounds[category_fitbounds_radio]" value="true" <?php echo $fitbounds ? checked( 'true', $fitbounds['category_fitbounds_radio'], false ) : false; ?> /></td>
										</tr>
										<tr>
											<td style="padding: 0 5px 5px"><label for="category_fitbounds_two">No</label></td>
											<td style="padding: 0 5px 5px"><input type="radio" id="category_fitbounds_two" name="category_fitbounds[category_fitbounds_radio]" value="false" <?php echo $fitbounds ? checked( 'false', $fitbounds['category_fitbounds_radio'], false ) : false; ?> /></td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr valign="top">
							<th scope="row">Map Colors:</th>
							<td class="map-colors">
								<div class="color-holder">
									<label>Highways</label>
									<p><span>Fill:</span> <input type="text" name="map_color_highway_fill" value="<?php echo esc_attr( get_option('map_color_highway_fill') ) ?: ""; ?>" class="neighborhood-map-color-field" /></p>
									<p><span>Outline:</span> <input type="text" name="map_color_highway_outline" value="<?php echo esc_attr( get_option('map_color_highway_outline') ) ?: ""; ?>" class="neighborhood-map-color-field" /></p>
								</div>
								<div class="color-holder">
									<label>Main Roads</label>
									<p><span>Fill:</span> <input type="text" name="map_color_road_city_fill" value="<?php echo esc_attr( get_option('map_color_road_city_fill') ) ?: ""; ?>" class="neighborhood-map-color-field" /></p>
									<p><span>Outline:</span> <input type="text" name="map_color_road_city_outline" value="<?php echo esc_attr( get_option('map_color_road_city_outline') ) ?: ""; ?>" class="neighborhood-map-color-field" /></p>
								</div>
								<div class="color-holder">
									<label>City Roads</label>
									<p><span>Fill:</span> <input type="text" name="map_color_road_arterial_fill" value="<?php echo esc_attr( get_option('map_color_road_arterial_fill') ) ?: ""; ?>" class="neighborhood-map-color-field" /></p>
									<p><span>Outline:</span> <input type="text" name="map_color_road_arterial_outline" value="<?php echo esc_attr( get_option('map_color_road_arterial_outline') ) ?: ""; ?>" class="neighborhood-map-color-field" /></p>
								</div>
								<div class="color-holder">
									<label>Roads Label</label>
									<p><span>Color:</span> <input type="text" name="map_color_road_label" value="<?php echo esc_attr( get_option('map_color_road_label') ) ?: ""; ?>" class="neighborhood-map-color-field" /></p>
								</div>
								<div class="color-holder">
									<label>State/Country Label</label>
									<p><span>Color:</span> <input type="text" name="map_color_state_label" value="<?php echo esc_attr( get_option('map_color_state_label') ) ?: ""; ?>" class="neighborhood-map-color-field" /></p>
								</div>
								<div class="color-holder">
									<label>Landscape</label>
									<p><span>Fill:</span> <input type="text" name="map_color_landscape" value="<?php echo esc_attr( get_option('map_color_landscape') ) ?: ""; ?>" class="neighborhood-map-color-field" /></p>
								</div>
								<div class="color-holder">
									<label>Water</label>
									<p><span>Fill:</span> <input type="text" name="map_color_water" value="<?php echo esc_attr( get_option('map_color_water') ) ?: ""; ?>" class="neighborhood-map-color-field" /></p>
								</div>
								<div class="color-holder">
									<label>Show POI?</label>
									<?php $show_poi = get_option( 'show_poi' ) ?: false; ?>
									<p>Yes:<span style="margin-right: 3px;"></span><input type="radio" id="show_poi_one" name="show_poi[show_poi_radio]" value="true" <?php echo $show_poi ? checked( 'true', $show_poi['show_poi_radio'], false ) : false; ?> /></p>
									<p>No: <span style="margin-right: 3px;"></span><input type="radio" id="show_poi_two" name="show_poi[show_poi_radio]" value="false" <?php echo $show_poi ? checked( 'false', $show_poi['show_poi_radio'], false ) : false; ?> /></p>
								</div>
							</td>
						</tr>
						<tr valign="top">
							<th scope="row">Filter Alignment</th>
							<td class="filter-alignment">
								<?php $filter_alignment = get_option( 'map_filter_alignment' ) ?: false; ?>
								<table>
									<tbody>
										<tr>
											<td style="padding: 0 5px 5px"><label for="filter_alignment_radio_one">Align Bottom</label></td>
											<td style="padding: 0 5px 5px"><input type="radio" id="filter_alignment_radio_one" name="map_filter_alignment[filter_alignment_radio]" value="bottom" <?php echo $filter_alignment ? checked( 'bottom', $filter_alignment['filter_alignment_radio'], false ) : false; ?> /></td>
										</tr>
										<tr>
											<td style="padding: 0 5px 5px"><label for="filter_alignment_radio_two">Align Right</label></td>
											<td style="padding: 0 5px 5px"><input type="radio" id="filter_alignment_radio_two" name="map_filter_alignment[filter_alignment_radio]" value="right" <?php echo $filter_alignment ? checked( 'right', $filter_alignment['filter_alignment_radio'], false ) : false; ?> /></td>
										</tr>
										<tr>
											<td style="padding: 0 5px 5px"><label for="filter_alignment_radio_three">Align Left</label></td>
											<td style="padding: 0 5px 5px"><input type="radio" id="filter_alignment_radio_three" name="map_filter_alignment[filter_alignment_radio]" value="left" <?php echo $filter_alignment ? checked( 'left', $filter_alignment['filter_alignment_radio'], false ) : false; ?> /></td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
				<table class="form-table default-location-settings col-sm-6">
					<thead><tr><th colspan="2"><h3>Default Location Settings</h3></th></tr></thead>
					<tbody>
						<tr valign="top">
							<th scope="row">Default Zoom</th>
							<td><p><em>Number range from 1 - 20</em></p><input type="text" name="default_zoom" value="<?php echo esc_attr( get_option('default_zoom') ) ?: '15'; ?>" /></td>
						</tr>
						<tr valign="top">
							<th scope="row">Default Tablet Zoom<br>
								<small><em>1024px - 768px</em></small>
							</th>
							<td><p><em>Number range from 1 - 20</em></p><input type="text" name="default_tablet_zoom" value="<?php echo esc_attr( get_option('default_tablet_zoom') ) ?: '13'; ?>" /></td>
						</tr>
						<tr valign="top">
							<th scope="row">Default Mobile Zoom<br>
								<small><em>600px</em></small>
							</th>
							<td><p><em>Number range from 1 - 20</em></p><input type="text" name="default_mobile_zoom" value="<?php echo esc_attr( get_option('default_mobile_zoom') ) ?: '11'; ?>" /></td>
						</tr>
						<tr valign="top">
							<th scope="row">Default Location<br>
								<small><em>Hotel Address</em></small></th>
							<td><input type="text" name="default_location_address" value="<?php echo esc_attr( get_option('default_location_address') ); ?>" /></td>
						</tr>
						<tr valign="top">
							<th scope="row">Default Location Lat/Lng</th>
							<td>
								<p><small>These will autofill if empty</small></p>
								<input type="text" name="default_location_lat" disabled value="<?php echo esc_attr( get_option('default_location_lat') ); ?>" /><br>
								<input type="text" name="default_location_lng" disabled value="<?php echo esc_attr( get_option('default_location_lng') ); ?>" />
							</td>
						</tr>
						<tr valign="top">
							<th scope="row">Default Location Title</th>
							<td><input type="text" name="default_location_title" value="<?php echo esc_attr( get_option('default_location_title') ); ?>" /></td>
						</tr>
						<tr valign="top">
							<th scope="row">Default Location Website</th>
							<td><input type="text" name="default_location_website" value="<?php echo esc_attr( get_option('default_location_website') ); ?>" /></td>
						</tr>
						<tr valign="top">
							<th scope="row">Default Location Phone Number</th>
							<td><input type="text" name="default_location_phone_number" value="<?php echo esc_attr( get_option('default_location_phone_number') ); ?>" /></td>
						</tr>
						<tr valign="top">
							<th scope="row">Default Location Icon</th>
							<td><?php neighborhood_map_image_uploader(); ?></td>
						</tr>
						<tr valign="top">
							<th scope="row">Default Location Icon Width (default: 50px)</th>
							<td><input type="text" name="default_location_width" value="<?php echo esc_attr( get_option('default_location_width') ); ?>" /></td>
						</tr>
						<tr valign="top">
							<th scope="row">Default Location Icon Height (default: 50px)</th>
							<td><input type="text" name="default_location_height" value="<?php echo esc_attr( get_option('default_location_height') ); ?>" /></td>
						</tr>
						<?php $country_border = get_option( 'country_border' ) ?: false; ?>
						<tr>
							<th scope="row">Show country border?</th>
						</tr>
						<tr>
							<td style="padding: 0 5px 5px"><label for="country_border_one" style="margin-right: 10px;">Yes</label><input type="radio" id="country_border_one" name="country_border[country_border]" value="true" <?php echo $country_border ? checked( 'true', $country_border['country_border'], false ) : false; ?> /></td>
						</tr>
						<tr>
							<td style="padding: 0 5px 5px"><label for="country_border_two" style="margin-right: 10px;">No</label><input type="radio" id="country_border_two" name="country_border[country_border]" value="false" <?php echo $country_border ? checked( 'false', $country_border['country_border'], false ) : false; ?> /></td>
						</tr>
					</tbody>
				</table>
			</div>

			<?php submit_button(); ?>

		</form>
	</div>
<?php
}

// include js and localize necessary variables
function load_neighborhood_scripts() {
	// Register the script
	wp_register_script( 'neighborhood_map_js', plugins_url().'/neighborhood-map/js/neighborhood-map.js', array('jquery'), 1.0, true );

	$default_location_data = [];

	if (get_option('default_location_address') != '') {
		$default_location_data = array(
			'title' => get_option('default_location_title') ?: '',
			'address' => get_option('default_location_address') ?: '',
			'website' => get_option('default_location_website') ?: '',
			'phone_number' => get_option('default_location_phone_number') ?: '',
			'icon' => wp_get_attachment_image_src(get_option('default_location_icon'), 'full')[0] ?: false,
			'icon_width' => get_option('default_location_width') ?: '50',
			'icon_height' => get_option('default_location_height') ?: '50',
			'lat' => get_option('default_location_lat') ?: '',
			'lng' => get_option('default_location_lng') ?: '',
			'zoom' => get_option('default_zoom') ?: 13,
			'show_poi' => get_option('show_poi') ?: false,
			'click_to_center' => get_option( 'click_to_center' ){'click_to_center_radio'} == 'yes' ? true : false,
			'constraints' => array(
				'lat' => array(
					'min' => get_option('min_lat') ?: false,
					'max' => get_option('max_lat') ?: false
				),
				'lng' => array(
					'min' => get_option('min_lng') ?: false,
					'max' => get_option('max_lng') ?: false
				),
			)
		);
	}

	$variables_to_localize = array(
		'api_key' => get_option('google_maps_api_key'),
		'scripts_path' => plugins_url().'/neighborhood-map/js/',
		'ajax_url' => admin_url('admin-ajax.php'),
		'default_location' => $default_location_data,
	);
	wp_localize_script( 'neighborhood_map_js', 'neighborhood', $variables_to_localize );

	// Enqueued script with localized data.
	wp_enqueue_script( 'neighborhood_map_js' );
}

add_action('wp_enqueue_scripts', 'load_neighborhood_scripts');

function get_neighborhood_locations() {
	$locations_array = [];
	$location_types = get_terms(array( 'taxonomy' => 'location-type' ));

	foreach ($location_types as $location) {
		$counter = 1;
		$args = array(
			'post_type' => 'locations',
			'post_status' => 'publish',
			'posts_per_page' => 100,
			'tax_query' => array(
				array(
					'taxonomy' => 'location-type',
					'field' => 'slug',
					'terms' => array($location->slug)
				),
			),
		);
		$locations = new WP_Query( $args );

		while ($locations->have_posts()) {
			$locations->the_post();

			$term = wp_get_post_terms(get_the_ID(), 'location-type')[0];

			$email_html = get_field('website') ? "<a style='margin-bottom: 5px; display: block' href='".get_field('website')."' target='_blank'>Visit Website</a>" : false;
			$phone_html = get_field('phone_number') ? "<a style='margin-bottom: 5px; display: block' href='tel:".get_field('phone_number')."'>".get_field('phone_number')."</a>" : false;
			$address_html = get_field('address') ? "<address><a href='https://maps.google.com/?q=".get_field('address')."' target='_blank'>".get_field('address')."</a></address>" : false;

			if (isset(get_field_objects()['city_state'])) {
				$address_html = "";

				if (get_field('city_state')) {
					$address_html = "<address><a>".get_field('city_state')."</a></address>";
				}
			}

			$locations_array[] = array(
				'post_id' => get_the_ID(),
				'id' => $counter,
				'title' => html_entity_decode(get_the_title()),
				'category' => wp_get_post_terms(get_the_ID(), 'location-type')[0]->slug,
				'description' => get_the_content(),
				'address' => get_field('address'),
				'website' => get_field('website'),
				'phone_number' => get_field('phone_number'),
				'lat' => get_field('latitude'),
				'lng' => get_field('longitude'),
				'category_color' => get_field('color', $term->taxonomy.'_'.$term->term_id),
				'category_icon' => get_field('icon', $term->taxonomy.'_'.$term->term_id) ? wp_get_attachment_image_src(get_field('icon', $term->taxonomy.'_'.$term->term_id))[0] : false,
				'infowindow_content' => ("
					<div id='info-content' style='line-height: 16px;'>
						<h6>".html_entity_decode(get_the_title())."</h6>
						$email_html
						$phone_html
						$address_html
						<div id='bodyContent'>".get_the_content()."</div>
					</div>
				"),
			);

			$counter++;
		}
	}

	wp_send_json($locations_array);
	wp_die(); // this is required to terminate immediately and return a proper response
}
//add admin ajax action for location lookup
add_action( 'wp_ajax_get_neighborhood_locations', 'get_neighborhood_locations' );
add_action( 'wp_ajax_nopriv_get_neighborhood_locations', 'get_neighborhood_locations' );
