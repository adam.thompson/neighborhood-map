<?php
//include cpt acf fields
include 'cpt-acf-fields.php';

//register locations cpt
function locations_init() {
	register_post_type( 'locations', array(
		'labels'                => array(
			'name'                  => __( 'Locations', 'YOUR-TEXTDOMAIN' ),
			'singular_name'         => __( 'Location', 'YOUR-TEXTDOMAIN' ),
			'all_items'             => __( 'All Locations', 'YOUR-TEXTDOMAIN' ),
			'archives'              => __( 'Location Archives', 'YOUR-TEXTDOMAIN' ),
			'attributes'            => __( 'Location Attributes', 'YOUR-TEXTDOMAIN' ),
			'insert_into_item'      => __( 'Insert into Location', 'YOUR-TEXTDOMAIN' ),
			'uploaded_to_this_item' => __( 'Uploaded to this Location', 'YOUR-TEXTDOMAIN' ),
			'featured_image'        => _x( 'Featured Image', 'locations', 'YOUR-TEXTDOMAIN' ),
			'set_featured_image'    => _x( 'Set featured image', 'locations', 'YOUR-TEXTDOMAIN' ),
			'remove_featured_image' => _x( 'Remove featured image', 'locations', 'YOUR-TEXTDOMAIN' ),
			'use_featured_image'    => _x( 'Use as featured image', 'locations', 'YOUR-TEXTDOMAIN' ),
			'filter_items_list'     => __( 'Filter Locations list', 'YOUR-TEXTDOMAIN' ),
			'items_list_navigation' => __( 'Locations list navigation', 'YOUR-TEXTDOMAIN' ),
			'items_list'            => __( 'Locations list', 'YOUR-TEXTDOMAIN' ),
			'new_item'              => __( 'New Location', 'YOUR-TEXTDOMAIN' ),
			'add_new'               => __( 'Add New', 'YOUR-TEXTDOMAIN' ),
			'add_new_item'          => __( 'Add New Location', 'YOUR-TEXTDOMAIN' ),
			'edit_item'             => __( 'Edit Location', 'YOUR-TEXTDOMAIN' ),
			'view_item'             => __( 'View Location', 'YOUR-TEXTDOMAIN' ),
			'view_items'            => __( 'View Locations', 'YOUR-TEXTDOMAIN' ),
			'search_items'          => __( 'Search Locations', 'YOUR-TEXTDOMAIN' ),
			'not_found'             => __( 'No Locations found', 'YOUR-TEXTDOMAIN' ),
			'not_found_in_trash'    => __( 'No Locations found in trash', 'YOUR-TEXTDOMAIN' ),
			'parent_item_colon'     => __( 'Parent Location:', 'YOUR-TEXTDOMAIN' ),
			'menu_name'             => __( 'Locations', 'YOUR-TEXTDOMAIN' ),
		),
		'public'                => get_option('single_location_page')['single_location_page_radio'] !== 'false' ? true : false,
		'hierarchical'          => false,
		'show_ui'               => true,
		'show_in_nav_menus'     => true,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'has_archive'           => true,
		'rewrite'               => true,
		'query_var'             => true,
		'menu_position'         => null,
		'menu_icon'             => 'dashicons-pressthis',
		'show_in_rest'          => true,
		'rest_base'             => 'locations',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
		'publicly_queryable'  	=> get_option('single_location_page')['single_location_page_radio'] !== 'false' ? true : false,
	) );
}
add_action( 'init', 'locations_init' );

/**
 * Registers the `location_type` taxonomy,
 * for use with 'locations'.
 */
function location_type_init() {
	register_taxonomy( 'location-type', array( 'locations' ), array(
		'hierarchical'      => true,
		'public'            => false,
		'show_in_nav_menus' => true,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => true,
		'capabilities'      => array(
			'manage_terms'  => 'edit_posts',
			'edit_terms'    => 'edit_posts',
			'delete_terms'  => 'edit_posts',
			'assign_terms'  => 'edit_posts',
		),
		'labels'            => array(
			'name'                       => __( 'Location types', 'resmark' ),
			'singular_name'              => _x( 'Location type', 'taxonomy general name', 'resmark' ),
			'search_items'               => __( 'Search Location types', 'resmark' ),
			'popular_items'              => __( 'Popular Location types', 'resmark' ),
			'all_items'                  => __( 'All Location types', 'resmark' ),
			'parent_item'                => __( 'Parent Location type', 'resmark' ),
			'parent_item_colon'          => __( 'Parent Location type:', 'resmark' ),
			'edit_item'                  => __( 'Edit Location type', 'resmark' ),
			'update_item'                => __( 'Update Location type', 'resmark' ),
			'view_item'                  => __( 'View Location type', 'resmark' ),
			'add_new_item'               => __( 'Add New Location type', 'resmark' ),
			'new_item_name'              => __( 'New Location type', 'resmark' ),
			'separate_items_with_commas' => __( 'Separate location types with commas', 'resmark' ),
			'add_or_remove_items'        => __( 'Add or remove location types', 'resmark' ),
			'choose_from_most_used'      => __( 'Choose from the most used location types', 'resmark' ),
			'not_found'                  => __( 'No location types found.', 'resmark' ),
			'no_terms'                   => __( 'No location types', 'resmark' ),
			'menu_name'                  => __( 'Location types', 'resmark' ),
			'items_list_navigation'      => __( 'Location types list navigation', 'resmark' ),
			'items_list'                 => __( 'Location types list', 'resmark' ),
			'most_used'                  => _x( 'Most Used', 'location-type', 'resmark' ),
			'back_to_items'              => __( '&larr; Back to Location types', 'resmark' ),
		),
		'show_in_rest'      => true,
		'rest_base'         => 'location-type',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	) );

}
add_action( 'init', 'location_type_init' );

function locations_type_columns( $taxonomies ) {
    $taxonomies[] = 'location-type';
    return $taxonomies;
}
add_filter( 'manage_taxonomies_for_locations_columns', 'locations_type_columns' );

/**
 * Sets the post updated messages for the `location_type` taxonomy.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `location_type` taxonomy.
 */
function location_type_updated_messages( $messages ) {

	$messages['location-type'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => __( 'Location type added.', 'resmark' ),
		2 => __( 'Location type deleted.', 'resmark' ),
		3 => __( 'Location type updated.', 'resmark' ),
		4 => __( 'Location type not added.', 'resmark' ),
		5 => __( 'Location type not updated.', 'resmark' ),
		6 => __( 'Location types deleted.', 'resmark' ),
	);

	return $messages;
}
add_filter( 'term_updated_messages', 'location_type_updated_messages' );


/**
 * Sets the post updated messages for the `locations` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `locations` post type.
 */
function locations_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['locations'] = array(
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf( __( 'Location updated. <a target="_blank" href="%s">View Location</a>', 'YOUR-TEXTDOMAIN' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'YOUR-TEXTDOMAIN' ),
		3  => __( 'Custom field deleted.', 'YOUR-TEXTDOMAIN' ),
		4  => __( 'Location updated.', 'YOUR-TEXTDOMAIN' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'Location restored to revision from %s', 'YOUR-TEXTDOMAIN' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		/* translators: %s: post permalink */
		6  => sprintf( __( 'Location published. <a href="%s">View Location</a>', 'YOUR-TEXTDOMAIN' ), esc_url( $permalink ) ),
		7  => __( 'Location saved.', 'YOUR-TEXTDOMAIN' ),
		/* translators: %s: post permalink */
		8  => sprintf( __( 'Location submitted. <a target="_blank" href="%s">Preview Location</a>', 'YOUR-TEXTDOMAIN' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf( __( 'Location scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Location</a>', 'YOUR-TEXTDOMAIN' ),
		date_i18n( __( 'M j, Y @ G:i', 'YOUR-TEXTDOMAIN' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		/* translators: %s: post permalink */
		10 => sprintf( __( 'Location draft updated. <a target="_blank" href="%s">Preview Location</a>', 'YOUR-TEXTDOMAIN' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'locations_updated_messages' );

/**
 * Add latitude/longitude to locations when saved with an address
 *
 * @param int $post_id The post ID.
 * @param post $post The post object.
 * @param bool $update Whether this is an existing post being updated or not.
 */
function add_location_geolocation_data( $post_id, $post, $update ) {
	$post_type = get_post_type($post_id);

    // If this isn't a 'locations' post, don't update it.
	if ( $post_type != "locations" ) return;
	// if we're deleting the post don't do this action
	if ( 'trash' == get_post_status( $post_id ) ) return;

	function get_the_lat_long($address){
		$url = "https://maps.google.com/maps/api/geocode/json?address=".urlencode($address)."&sensor=false&key=".get_option('google_maps_api_key');

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$response = curl_exec($ch);
		curl_close($ch);

		$json = json_decode($response);

		$lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
		$long = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
		return array('lat' => $lat, 'lng' => $long);
	}

	$address = get_field('address', $post_id);
	//if our address isn't blank let's get latitude and longitude using our handy function above
	if ($address != '') {
		$lat_lng_array = get_the_lat_long($address);
		//if latitude blank let's fill it in
		if (get_field('latitude', $post_id) == '') {
			update_field('latitude', $lat_lng_array['lat']);
		}
		//if longitude blank let's fill it in
		if (get_field('longitude', $post_id) == '') {
			update_field('longitude', $lat_lng_array['lng']);
		}
	}
}

add_action( 'save_post', 'add_location_geolocation_data', 10, 3 );
