//neighborhood has been localized
(function($) {
	if ($('.google-map').length) {
		let customStyles = JSON.parse($('#map-stylings').html());

		let mapstyle = [
			{"featureType":"water",
				"elementType":"geometry.fill",
				"stylers":[
				customStyles.water ? {"color":customStyles.water} : {"visibility":"off"}
			]},
			{"featureType":"water",
			"elementType":"labels.text.fill",
			"stylers":[
				customStyles.road_label ? {"color":customStyles.road_label} : {"visibility":"off"}
			]},
			{"featureType":"transit",
			"stylers":[
				{"color":"#808080"},
				{"visibility":"off"}
			]},
			{"featureType":"road.highway",
			"elementType":"geometry.stroke",
			"stylers":[
				customStyles.highway_outline ? {"color":customStyles.highway_outline} : {"visibility":"off"}
			]},
			{"featureType":"road.highway",
			"elementType":"geometry.fill",
			"stylers":[
				customStyles.highway_fill ? {"color":customStyles.highway_fill} : {"visibility":"off"}
			]},
			{"featureType":"road.local",
			"elementType":"geometry.fill",
			"stylers":[
				customStyles.road_city_fill ? {"color":customStyles.road_city_fill} : {"visibility":"off"},
				{"weight":1.8}
			]},
			{"featureType":"road.local",
			"elementType":"geometry.stroke",
			"stylers":[
				customStyles.road_city_outline ? {"color":customStyles.road_city_outline} : {"visibility":"off"}
			]},
			{"featureType":"road.arterial",
			"elementType":"geometry.fill",
			"stylers":[
				customStyles.road_arterial_fill ? {"color":customStyles.road_arterial_fill} : {"visibility":"off"}
			]},
			{"featureType":"road.arterial",
			"elementType":"geometry.stroke",
			"stylers":[
				customStyles.road_arterial_outline ? {"color":customStyles.road_arterial_outline} : {"visibility":"off"}
			]},
			{"featureType":"road",
			"elementType":"labels.text.fill",
			"stylers":[
				customStyles.road_label ? {"color":customStyles.road_label} : {"visibility":"off"}
			]},
			{"featureType":"road",
			"elementType":"labels.icon",
			"stylers":[
				{"visibility":"off"}
			]},
			{"featureType":"landscape",
			"elementType":"geometry.fill",
			"stylers":[
				customStyles.landscape ? {"color":customStyles.landscape} : {"visibility":"off"}
			]},
			{"featureType":"administrative.locality",
			"elementType":"labels.text.fill",
			"stylers":[
				customStyles.state_label ? {"color":customStyles.state_label} : {"visibility":"off"},
			]},
			{"featureType":"administrative",
			"elementType":"geometry",
			"stylers":[
				{"visibility":"off"},
			]},
			{"featureType":"administrative",
			"elementType":"geometry",
			"stylers":[
				{"visibility":"off"},
			]},
			{"featureType":"administrative.locality",
			"elementType": "labels.icon",
			"stylers": [
				{"visibility":"off"}
			]},
			{"featureType":"administrative.country",
			"elementType":"geometry.stroke",
			"stylers":[
				customStyles.country_border != 'false' ? {"visibility":"on"} : {"visibility":"off"},
			]},
			{"featureType":"landscape.natural",
			"elementType":"labels",
			"stylers":[
				{"visibility":"off"},
			]},
			{"featureType":"poi",
			"elementType":"geometry.fill",
			"stylers":[
				customStyles.show_poi != 'false' ? {"visibility":"on"} : {"visibility":"off"},
				customStyles.show_poi != 'false' ? {"saturation":"-100"} : {"saturation":"100"},
			]},
			{"featureType":"poi",
			"elementType":"labels.icon",
			"stylers":[
				customStyles.show_poi != 'false' ? {"visibility":"on"} : {"visibility":"off"},
				customStyles.show_poi != 'false' ? {"saturation":"-100"} : {"saturation":"100"},
			]},
			{"featureType":"poi",
			"elementType":"labels",
			"stylers":[
				customStyles.show_poi != 'false' ? {"visibility":"on"} : {"visibility":"off"},
				customStyles.show_poi != 'false' ? {"saturation":"-100"} : {"saturation":"100"},
			]},
			{"featureType":"poi",
			"elementType":"geometry.fill",
			"stylers":[
				{"color":"#dadada"}
			]}
		];

		let loadMap = function(el, pins, options, callback) {
			var api_key = neighborhood.api_key;
			options = {};

			$.getScript('https://maps.google.com/maps/api/js?key='+api_key, function() {
				$.getScript(neighborhood.scripts_path+'infobox.js', function() {
					renderMap();
				});
			});

			function renderMap() {
				mapsLoaded = true;
				let setcenter = false;
				if (neighborhood.default_location.lat && neighborhood.default_location.lng) {
					setcenter = true;
					options.center = new google.maps.LatLng(neighborhood.default_location.lat, neighborhood.default_location.lng);
				} else {
					options.center = new google.maps.LatLng('34.041686','-118.2662567');
				}
				options.mapTypeId = options.mapTypeId || google.maps.MapTypeId.ROADMAP;
				options.styles = options.styles || mapstyle;
				// zoom levels
				options.zoom = 15;
				options.zoom = $('.google-map').data('zoom') ? $('.google-map').data('zoom') : parseInt(customStyles.zoom);
				// if on tablet get tablet level zoom
				if ($(window).width() <=1024 && $(window).width >= 768) {
					options.zoom = $('.google-map').data('zoom') ? $('.google-map').data('zoom') : parseInt(customStyles.tablet_zoom);
				}
				// if on mobile get mobile level zoom
				if ($(window).width() < 768) {
					options.zoom = $('.google-map').data('zoom') ? $('.google-map').data('zoom') : parseInt(customStyles.mobile_zoom);
				}

				let map = new google.maps.Map($(el)[0], options);
				let markers = [];
				let infoboxes = [];
				let bounds = new google.maps.LatLngBounds();

				//make function to just set default marker here
				set_default_location_marker(map, markers, infoboxes);

				// if our shortcode doesn't have show-locations as false get our location markers
				if ($('.section.google-map').data('show-locations') != false) {
					get_location_markers(map, markers, infoboxes, bounds);
				}

				if (!options.zoom) {
					map.fitBounds(bounds);
					if (setcenter) {
						google.maps.event.addListenerOnce(map, "center_changed", function() {
							map.setCenter(options.center);
						});
					}

					google.maps.event.addListenerOnce(map, "zoom_changed", function() {
						if (map.getZoom() > 15) {
							map.setZoom(15);
						}
					});
				}

				if (callback) {
					callback({
						map: map,
						markers: markers,
						infoboxes: infoboxes,
						bounds: bounds,
					});
				}

				//custom constraint listenders
				if (neighborhood.default_location.constraints) {
					if (neighborhood.default_location.constraints.lat.max && neighborhood.default_location.constraints.lat.min &&
						neighborhood.default_location.constraints.lng.max && neighborhood.default_location.constraints.lng.min) {
						var allowedBounds = new google.maps.LatLngBounds(
							new google.maps.LatLng(neighborhood.default_location.constraints.lat.min, neighborhood.default_location.constraints.lng.min),
							new google.maps.LatLng(neighborhood.default_location.constraints.lat.max, neighborhood.default_location.constraints.lng.max),
						);
						var lastValidCenter = map.getCenter();

						google.maps.event.addListener(map, 'dragend', function() {
							if (allowedBounds.contains(map.getCenter())) {
								// still within valid bounds, so save the last valid position
								lastValidCenter = map.getCenter();
								return;
							}

							// not valid anymore => return to last valid position
							map.panTo(lastValidCenter);
						});
					}
				}
			}
		};

		function mapLoaded(mapdata) {
			$('.section.google-map .map-filters li #location-heading').on('click', function() {
				let category = $(this).parent().attr('id');
				let selectedBounds = new google.maps.LatLngBounds();

				$('.section.google-map .map-filters li #location-heading').removeClass('active');
				//if filters aligned bottom
				if ($('.section.google-map').hasClass('filter-bottom')) {
					$('.section.google-map .map-filters li #location-heading').removeClass('filtered');
					$('.section.google-map .map-filters').addClass('filtered');
					$('.section.google-map .map-filters li').removeClass('active');
					$(this).addClass('active').parent().addClass('active');
				} else {
					$('.section.google-map .map-filters li #location-list').removeClass('active');
					$(this).addClass('active').next().addClass('active');
				}

				//if all selected
				if (category === 'All') {
					$('.section.google-map .map-filters li').removeClass('active');
					$('.section.google-map .map-filters').removeClass('filtered');
					//if filters aligned bottom
					if ($('.section.google-map').hasClass('filter-bottom')) {
						for (let i = 0; i < mapdata.markers.length; i++) {
							mapdata.markers[i].setVisible(true);
							selectedBounds.extend(mapdata.markers[i].position);
						}
					} else {
						$('.section.google-map .map-filters li #location-heading').addClass('active');
						$('.section.google-map .map-filters li #location-list').addClass('active');
						for (let i = 0; i < mapdata.markers.length; i++) {
							mapdata.markers[i].setVisible(true);
							selectedBounds.extend(mapdata.markers[i].position);

							if (mapdata.infoboxes[i].getVisible()) {
								mapdata.infoboxes[i].close(mapdata.map, mapdata.markers[i]);
							}
						}
					}
				} else {
					//if individual category selected
					for (let i = 0; i < mapdata.markers.length; i++) {
						if (mapdata.infoboxes[i].getVisible()) {
							mapdata.infoboxes[i].close(mapdata.map, mapdata.markers[i]);
						}

						if (mapdata.markers[i].category.indexOf(category) >= 0) {
							mapdata.markers[i].setVisible(true);

							selectedBounds.extend(mapdata.markers[i].position);

						} else {
							mapdata.markers[i].setVisible(false);
						}
					}

					if (customStyles.fitbounds != 'false') {
						mapdata.map.fitBounds(selectedBounds);
					}
				}
			});

			let newZoom = $('.google-map').data('zoom') ? $('.google-map').data('zoom') : parseInt(customStyles.zoom);

			$(window).on('resize', function() {
				// if on tablet get tablet level zoom
				if ($(window).width() <= 1024 && $(window).width >= 768) {
					newZoom = $('.google-map').data('zoom') ? $('.google-map').data('zoom') : parseInt(customStyles.tablet_zoom);
				}
				// if on mobile get mobile level zoom
				if ($(window).width() < 768) {
					newZoom = $('.google-map').data('zoom') ? $('.google-map').data('zoom') : parseInt(customStyles.mobile_zoom);
				}

				setTimeout(() => {
					mapdata.map.setZoom(newZoom);
				}, 500);
			});
		}

		let mapoptions = {
			zoom: 15,
		};

		loadMap($('.section.google-map .map'), false, mapoptions, mapLoaded);

		//function to setup the default location setup in the plugin settings
		function set_default_location_marker(map, markers, infoboxes) {
			let defaultMarker = new google.maps.Marker({
				position: new google.maps.LatLng(Number(neighborhood.default_location.lat), Number(neighborhood.default_location.lng)),
				map: map,
				icon: {
					url: neighborhood.default_location.icon,
					scaledSize: new google.maps.Size(parseInt(neighborhood.default_location.icon_width), parseInt(neighborhood.default_location.icon_height)),
					origin: new google.maps.Point(0, 0),
					anchor: new google.maps.Point(parseInt(neighborhood.default_location.icon_width)/2, parseInt(neighborhood.default_location.icon_height)/2),
				},
				info: neighborhood.default_location.address,
				title: neighborhood.default_location.title,
				category: 'all',
			});
			// Create infobox from marker.
			let infoboxdata = {
				content: `<div id='info-content' style="line-height: 18px;">
					<h6>${neighborhood.default_location.title}</h6>
					<a style='margin-bottom: 5px;' href='tel:${neighborhood.default_location.phone_number}'>${neighborhood.default_location.phone_number}</a>
					<address><a aria-label='opens a new tab' href='https://maps.google.com/?q=${neighborhood.default_location.address}' target='_blank'>${neighborhood.default_location.address}</address>
				</div>`,
				alignBottom: true,
				pixelOffset: new google.maps.Size(12, -36),
			};
			let infobox = new InfoBox(infoboxdata);

			google.maps.event.addListener(defaultMarker, 'click', function() {
				let show = !infobox.getVisible();
				if (show) {
					infobox.open(map, defaultMarker);

					for (let i in markers) {
						if (infoboxes[i].getVisible()) {
							infoboxes[i].close(map, markers[i]);
						}
					}
				}
			});
		}

		function get_location_markers(map, markers, infoboxes, bounds) {
			let data = {'action': 'get_neighborhood_locations'};

			$.post(neighborhood.ajax_url, data, function(response) {
				// Create individual marker$(s from map data, attaching info boxes as needed.
				response.map(response => {
					//if we have a filter-by-slug in the shortcode
					if ($('.section.google-map').data('filter') != '') {
						let slug = $('.section.google-map').data('filter');
						if (response.category.indexOf(slug) != false) {
							return false;
						}
					}
					// Create marker from data and store in array.
					let markerIcon = {
						path: "M15 0a15 15 0 1 1-30 0 15 15 0 1 1 30 0z",
						fillColor: response.category_color,
						fillOpacity: 0.8,
						scale: 1,
						strokeColor: response.category_color,
						strokeWeight: 0,
					};
					let markerLabel = {
						text: response.id.toString(),
						color: "#ffffff",
						fontSize: "16px",
						fontWeight: "bold"
					};

					if (customStyles.location_style == 'icon') {
						markerIcon = {
							url: response.category_icon,
							scaledSize: new google.maps.Size(24, 36),
							origin: new google.maps.Point(0, 0),
							anchor: new google.maps.Point(12, 18),
						}

						markerLabel = {text: " "};
					}

					let markerdata = {
						position: new google.maps.LatLng(Number(response.lat), Number(response.lng)),
						map: map,
						label: markerLabel,
						icon: markerIcon,
						id: response.post_id,
						info: response.description,
						title: response.title,
						category: response.category,
					};
					let marker = new google.maps.Marker(markerdata);
					markers.push(marker);

					// Create infobox from marker.
					let infoboxoffset = new google.maps.Size(12, -36);
					let infoboxdata = {
						content: response.infowindow_content,
						alignBottom: true,
						disableAutoPan: neighborhood.default_location.click_to_center == false ? true : false,
						pixelOffset: infoboxoffset,
					};
					let infobox = new InfoBox(infoboxdata);
					infoboxes.push(infobox);

					// When a marker is clicked, hide other infoboxes, and show the one attached to the current marker.
					google.maps.event.addListener(marker, 'click', function(e) {
						let show = !infobox.getVisible();
						for (let i in markers) {
							if (infoboxes[i].getVisible()) {
								infoboxes[i].close(map, markers[i]);
							}
						}
						if (show) {
							infobox.open(map, marker);

							// if the click to center option is set to true
							if (neighborhood.default_location.click_to_center) {
								map.panTo(marker.getPosition());
							}
						}

						//if the map has a category chosen
						if ($('.section.google-map .map-filters').hasClass('filtered')) {
							$('#location-list .map-location').removeClass('active');
							$('#location-list .map-location').each(function() {
								if ($(this).attr('id') == marker.id) {
									$(this).addClass('active');
								}
							});
						}
					});

					$('#location-list .map-location').on('click', function() {
						let ID = $(this).attr('id');

						if ($('.section.google-map').hasClass('filter-bottom')) {
							//if the map has a category chosen
							if ($('.section.google-map .map-filters').hasClass('filtered')) {
								if ($(this).parent().prev().hasClass('active')) {
									for (let i in markers) {
										if (infoboxes[i].getVisible()) {
											infoboxes[i].close(map, markers[i]);
										}

										if (markers[i].id == ID) {
											infoboxes[i].open(map, markers[i]);

											if (neighborhood.default_location.click_to_center) {
												map.setCenter(markers[i].getPosition());
											}
										}
									}
								}
							} else {
								for (let i in markers) {
									if (infoboxes[i].getVisible()) {
										infoboxes[i].close(map, markers[i]);
									}

									if (markers[i].id == ID) {
										infoboxes[i].open(map, markers[i]);
										map.setCenter(markers[i].getPosition());
									}
								}
							}
						} else {
							if ($(this).parent().prev().hasClass('active')) {
								for (let i in markers) {
									if (infoboxes[i].getVisible()) {
										infoboxes[i].close(map, markers[i]);
									}

									if (markers[i].id == ID) {
										infoboxes[i].open(map, markers[i]);
										map.setCenter(markers[i].getPosition());
									}
								}
							}
						}
					});
				});
			});
		}
	}
})(jQuery);
